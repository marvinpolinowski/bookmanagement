// Editor: Paul Bergmann

import Vanderlande.BookManagement.Data.BookDAO;
import Vanderlande.BookManagement.Logic.BookManagement;
import Vanderlande.BookManagement.UI.Controller;

public class MainApplication
{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BookManagement bookManagement = new BookManagement(
				new BookDAO());
		Controller controller = new Controller(bookManagement);
		controller.start();
	}
}

