package Vanderlande.BookManagement.Data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Vanderlande.BookManagement.Logic.Book;

/**
 *
 */
public class BookDAO implements IBookDAO
{

	/**
	 * Initialisiert die B�cherliste, da noch keine Persistenz bekannt
	 * 
	 * @param liste
	 *            Die Struktur, in die die B�cher geladen werden sollen
	 * @return Gibt false zur�ck, wenn ein Buch nicht in liste gespeichert
	 *         werden konnte sonst true
	 */
	private boolean init(List<Book> liste) {
		boolean result = true;
		result = result & liste.add(new Book("Handbuch der Java-Programmierung", 52.5f));
		result = result & liste.add(new Book("Core-Java Volume 1", 72.9f));
		result = result & liste.add(new Book("Softwaretechnik", 65.45f));
		result = result & liste.add(new Book("Grundkurs Programmieren in Java", 50.99f));

		// M�sste in BuecherVerwaltung zu Exception f�hren
		// result = result & liste.add(new Buch(1, "Kain und Abel", 47.88f)); // Doppelte ID 1
		return result;

	}

	/**
	 * "L�dt" die B�cherliste aus dem Persistenzmedium und gibt sie an den
	 * Aufrufer zur�ck
	 * 
	 * @return Menge mit den geladenen B�chern
	 * @throws IOException
	 */
	public List<Book> load() throws IOException {
		List<Book> list = new ArrayList<Book>();
		if (!init(list)) // Da noch keine IO bekannt
			throw new IOException("Fehler beim Laden der Buchdaten");
		return list;
	}

	/**
	 * persists the data in the list
	 * 
	 * @param load
	 *            list of book to be persisted
	 * @throws IOException
	 */
	public void save(List<Book> load) throws IOException {
		// Rumpf leer, da noch keine IO bekannt
		// throw new IOException("Daten konnten nicht gespeichert werden");
	}
}
