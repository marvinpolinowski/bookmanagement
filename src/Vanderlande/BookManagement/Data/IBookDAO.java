package Vanderlande.BookManagement.Data;

import Vanderlande.BookManagement.Logic.Book;

import java.io.IOException;
import java.util.List;

/**
 * Ein Interface fuer ein Buch-DAO. Zur Vereinfachung wird ueber dieses
 * Interface immer der komplette Datenbestand gelesen und geschrieben.
 */
public interface IBookDAO
{
	List<Book> load() throws IOException;

	void save(List<Book> load) throws IOException;
}
