package Vanderlande.BookManagement.UI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @version 5.0
 *
 **/                                
@SuppressWarnings("serial")
public class BookMainView extends JFrame {

	private Controller controller;

	private JButton load;
	private JButton save;
	private JButton edit;

	private JPanel buttonPanel; // Zur Aufnahme der Buttons

	public BookMainView(Controller controller) {
		super("BookManagement");
		this.controller = controller;
		setSize(650, 300);
		setMinimumSize(new Dimension(650, 300));
		setLocationRelativeTo(null);

		initButtonPanel();
	    add(buttonPanel, BorderLayout.NORTH);

		iniButtons();
		
		// Beobachter registrieren
		registerListeners();
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);	
		setVisible(true);
	}

	private void initButtonPanel() {
		
		buttonPanel = new JPanel();

		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 2, 2));

		load = createButton("load");
		buttonPanel.add(load);
	
		save = createButton("save");
		buttonPanel.add(save);
	
		edit = createButton("edit");
		buttonPanel.add(edit);
	}
	
	private JButton createButton (String text) {
		JButton b = new JButton(text);
		b.setPreferredSize(new Dimension(95, 30));
		return b;
	}
	
	// V5.0: private -> Paketglobal, da Controllercode ausgelagert
	void setupEditButtons() {
		load.setEnabled(false);
		save.setEnabled(true);
		edit.setEnabled(false);
	}

	// V5.0: private -> Paketglobal, da Controllercode ausgelagert
	void iniButtons() {
		load.setEnabled(true);
		save.setEnabled(false);
		edit.setEnabled(true);
	}

	/*********** Verbindung zwischen View und Controllerteil ***********/
	// Beobachter an den Buttons registrieren
	private void registerListeners() {
		Controller.MainViewListener mvL = controller.createMainViewListener();
		load.addActionListener(mvL);
		save.addActionListener(mvL);
		edit.addActionListener(mvL);
		getContentPane().addContainerListener(mvL);
	}
}
