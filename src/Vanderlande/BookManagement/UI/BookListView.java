package Vanderlande.BookManagement.UI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JList;
import javax.swing.ListSelectionModel;

import Vanderlande.BookManagement.Logic.Book;

/**
 * @version 5.0
 *
 **/  
@SuppressWarnings("serial")
public class BookListView extends JInternalFrame {

	private JList<Book> showList;
	private DefaultListModel<Book> listModel;

	public BookListView(Controller controller, java.util.List<Book> buchliste) {

		super("Book list", false, // resizable
				true, // closable
				false, // maximizable
				false);// iconifiable

		setLayout(new BorderLayout());

		setSize(500, 200);

		Box buttonPanel = new Box(BoxLayout.Y_AXIS);

		JButton neu = createButton("new");
		JButton aendern = createButton("edit");

		buttonPanel.add(neu);
		buttonPanel.add(aendern);

		add("East", buttonPanel);

		listModel = new DefaultListModel<Book>();
		showList = new JList<Book>(listModel);
		showList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		showList.setFont(new Font("Consolas", Font.PLAIN + Font.BOLD,14));
		showList.setEnabled(true);

		add(showList, BorderLayout.CENTER);
		fillBookList(buchliste);

		
		Controller.BookListListener blL = controller.createBookListListener(showList, listModel);
		neu.addActionListener(blL);
		aendern.addActionListener(blL);
		addInternalFrameListener(blL);

		setVisible(true);
	}

	private JButton createButton(String text) {
		JButton b = new JButton(text);
		b.setMinimumSize(new Dimension(90, 30));
		b.setPreferredSize(new Dimension(90, 30));
		b.setMaximumSize(new Dimension(90, 30));
		return b;
	}

	private void fillBookList(java.util.List<Book> buchliste) {
		for (Book b : buchliste) {
			listModel.addElement(b);
		}
	}
}
