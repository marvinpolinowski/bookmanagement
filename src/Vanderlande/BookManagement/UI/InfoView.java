package Vanderlande.BookManagement.UI;


import java.awt.Window;

import javax.swing.JOptionPane;

public class InfoView {

	public InfoView(Window window, String info) {
		JOptionPane.showMessageDialog(window, info);
	}
}
