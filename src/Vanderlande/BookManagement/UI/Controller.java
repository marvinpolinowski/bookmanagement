package Vanderlande.BookManagement.UI;

import Vanderlande.BookManagement.Logic.Book;
import Vanderlande.BookManagement.Logic.BookExistingException;
import Vanderlande.BookManagement.Logic.BookManagement;
import Vanderlande.BookManagement.Logic.BookNotFoundException;

import javax.swing.*;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.List;

public class Controller {
	private BookManagement bookManagement;
	private BookMainView ui;
	private BookListView blv;

	private CreatingBookView beV;
	private boolean bookSaved;

	public Controller(BookManagement bookManagement) {
		this.bookManagement = bookManagement;
	}

	/**
	 * Erzeugt das BuchHauptprogrammView-Objekt und startet das Hauptmenü
	 */
	public void start() {
		ui = new BookMainView(this);
		new InfoView(ui, "Start of the BookManagement!");
	}

	/**
	 * 
	 * @throws BookExistingException
	 * @return Buch Rückgabe ist null, wenn Bucherfassung abgebrochen wurde.
	 *         Wichtig für BuchListeView, um zu wissen, ob Buch der angezeigten
	 *         Liste hinzugefügt werden soll oder nicht
	 */
	public Book create() throws BookExistingException
	{
		Book buch = new Book();
		beV = new CreatingBookView(ui, buch, this);
		beV.setVisible(true);

		// Ausf�hrung kehrt hierhin erst zur�ck, wenn Dialog geschlossen oder
		// nicht mehr sichtbar ist.
		if (bookSaved) {
			bookManagement.add(buch);
			bookSaved = false;
			return buch;
		}
		return null;
	}

	/**
	 * Die Daten des durch seine ID identifizierten Buchs werden geändert. Dafür
	 * wird die BuchErfassungView erzeugt.
	 * 
	 * @param id
	 *            ID des zu ändernden Buches
	 * @throws BookNotFoundException
	 */
	public void changeBook(long id) throws BookNotFoundException
	{
		Book b = bookManagement.getBook(id);
		beV = new CreatingBookView(ui, b, this);
		beV.setVisible(true);
	}

	/**
	 * Die View-Objekt für die Buchliste wird erzeugt und das Bearbeiten der
	 * Liste gestartet
	 * 
	 * @throws BookNotFoundException
	 */
	public void addListToUI() throws BookNotFoundException
	{
		// Es wird ein internes Fenster erzeugt zum Anzeigen,
		// Neuerfassen und ändern von Büchern. Dieses Fenster wird
		// dem Hauptfenster hinzugefogt
		blv = new BookListView(this, bookManagement.getBookList());
		ui.add(blv);
	}

	/**
	 * In der Bücherverwaltung wird das Laden der Bücher angestoßen
	 * 
	 * @throws IOException
	 * @throws BookExistingException
	 */
	public void load() throws IOException, BookExistingException {
		bookManagement.load();
	}

	/**
	 * In der Bücherverwaltung wird das Speichern der Bücher angestoßen
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException {
		bookManagement.save();
	}

	public List<Book> getBookList() {
		return bookManagement.getBookList();
	}

	/**** Beobachterobjekte vom Typ der inneren Klassen erzeugen ******/
	public BookListListener createBookListListener(JList<Book> showList,
												   DefaultListModel<Book> ListModel) {
		return new BookListListener(showList, ListModel);
	}

	public MainViewListener createMainViewListener() {
		return new MainViewListener();
	}

	public BookCreationListener createBookCreationListener(JTextField tf_titel,
			                                      JTextField tf_preis, Book book) {
		return new BookCreationListener(tf_titel, tf_preis, book);
	}

	/***********************************************************/
	/** Controllerteile der einzelnen GUIs als innere Klassen **/
	/***********************************************************/

	/**** Listener für BuchListeView *********/
	class BookListListener extends InternalFrameAdapter implements ActionListener {

		private JList<Book> showList;
		private DefaultListModel<Book> listModel;

		public BookListListener(JList<Book> showList, DefaultListModel<Book> listModel) {
			this.showList = showList;
			this.listModel = listModel;
		}

		public void actionPerformed(ActionEvent evt) {
			String action = evt.getActionCommand();
			try {
				if (action.equals("new")) {
					Book b = create();
					if (b != null) {
						listModel.addElement(b);
					}
					showList.clearSelection();
				}
				if (action.equals("edit")) {
					int selected = showList.getSelectedIndex();
					if (selected >= 0) {
						Book buch = listModel.get(selected);
						changeBook(buch.id);
						listModel.set(selected, buch);
						showList.clearSelection();
					} else
						new InfoView(ui, "Kein Buch ausgew�hlt");
				}
			} catch (BookExistingException | BookNotFoundException e) {
				new InfoView(ui, e.getMessage());
			}
		}

		public void internalFrameClosing(InternalFrameEvent evt) {
			blv.dispose();
			ui.remove(blv);
		}
	}

	/**** Listener für BuchHauptprogrammView *********/
	class MainViewListener implements ActionListener, ContainerListener {

		public void actionPerformed(ActionEvent e) {
			String action = e.getActionCommand();
			try {
				switch (action) {
				case "load":
					load();
					new InfoView(ui, "Data was loaded");
					break;
				case "save":
					save();
					new InfoView(ui, "Data was saved");
					break;
				case "edit":
					ui.setupEditButtons();
					addListToUI();
					break;
				}
			} catch (IOException | BookNotFoundException | BookExistingException e1) {
				new InfoView(ui, e1.getMessage());
			}
		}

		public void componentAdded(ContainerEvent evt) {
		}

		public void componentRemoved(ContainerEvent evt) {
			ui.iniButtons();
		}
	}

	/**** Listener für BuchErfassungView *********/
	class BookCreationListener extends KeyAdapter implements ActionListener {

		private Book book;
		private JTextField tf_title;
		private JTextField tf_price;

		public BookCreationListener(JTextField tf_title, JTextField tf_price, Book book) {
			this.tf_title = tf_title;
			this.tf_price = tf_price;
			this.book = book;
		}

		private void close() {
			beV.setVisible(false);
			beV.dispose();
		}
		
		private void save() {
			try {
				String name = tf_title.getText();
				if (name == null || name.equals("")) {
					bookSaved = false;
					tf_title.requestFocus();
					new InfoView(beV, "Please enter a valid title.");
				} else {
					book.title=name;
					float price = Float.parseFloat(tf_price.getText());
					book.price=price;
					bookSaved = true;
					close();
				}
			} catch (NumberFormatException e) {
				bookSaved = false;
				tf_price.requestFocus();
				new InfoView(beV, "Please enter a valid amount");
			}
		}

		public void actionPerformed(ActionEvent e) {
			String action = e.getActionCommand();

			if (action.equals("Save")) {
				save();
			}
			if (action.equals("Cancel")) {
				close();
			}
		}

		public void keyPressed(KeyEvent evt) {
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
				Component c = evt.getComponent();
				if (c instanceof JButton) {
					String action = ((JButton) c).getActionCommand();
					if (action.equals("Save")) {
						save();
					}
					if (action.equals("Cance")) {
						close();
					}
				} else
					beV.focusToNextComponent();
			}
		}

	}
}
