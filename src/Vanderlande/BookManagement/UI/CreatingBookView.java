package Vanderlande.BookManagement.UI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Vanderlande.BookManagement.Logic.Book;
/**
 * @version 5.0
 * Listener nu in de controller klasse gedefinieerd
 * 
 **/  
@SuppressWarnings("serial")
public class CreatingBookView extends JDialog {

	private Book book;
	private JTextField tf_title;
	private JTextField tf_price;

	//to be renamed
	private JButton button_ok;
	private JButton button_cancel;
	
	public CreatingBookView(JFrame mainwindow, Book book, Controller controller) {
		super(mainwindow, "Boek catalogus", true);
		this.book = book;
		setSize(290, 150);
		Point p = mainwindow.getLocationOnScreen();
		setLocation(p.x + 100, p.y + 80);
				
		JPanel textFieldPWr = new JPanel();
		textFieldPWr.add(createJTextFieldJPanel());
		add(textFieldPWr, BorderLayout.CENTER);
		add(createJButtonJPanel(), BorderLayout.SOUTH);
		
		Controller.BookCreationListener beL = controller.createBookCreationListener(tf_title, tf_price, book);

		tf_title.addKeyListener(beL);
		tf_price.addKeyListener(beL);
		button_ok.addActionListener(beL);
		button_cancel.addActionListener(beL);
		button_ok.addKeyListener(beL);
		button_cancel.addKeyListener(beL);
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//		setVisible(true); Oproep verplaatst naar Controller
	}

	private JPanel createJTextFieldJPanel() {
		JPanel textFieldPanel = new JPanel();
		textFieldPanel.setLayout(new GridLayout(2, 1));

		// JPanel met Label en Textveld voor de titel
		JPanel ptop = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		ptop.add(new JLabel("Titel:"));
		tf_title = new JTextField(15);
		ptop.add(tf_title);

		// JPanel met Label en Textveld voor de prijs
		JPanel pbottom = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		tf_price = new JTextField(15);
		pbottom.add(new JLabel("Prijs:"));
		pbottom.add(tf_price);
		
		if (book != null) {
			update();
		}
        // add both titel and price to the text field
		// beide JPanel voor titel en prijs aan textFieldPanel toevoegen
		textFieldPanel.add(ptop);
		textFieldPanel.add(pbottom);
		
		return textFieldPanel;
	}

	private JPanel createJButtonJPanel() {
		JPanel buttonJPanel = new JPanel();

		button_ok = new JButton("Save");
		button_cancel = new JButton("Cancel");
		
		buttonJPanel.add(button_ok);
		buttonJPanel.add(button_cancel);
			
		return buttonJPanel;
	}

	// Gegevens van het boek overnemen in de Text ingave velden
	private void update() {
		tf_title.setText(book.title);
		tf_price.setText(String.valueOf(book.price));
	}
	
	void focusToNextComponent() {
		Component c = this.getFocusOwner();
		if (c == tf_title)
			tf_price.requestFocus();
		if (c == tf_price)
				button_ok.requestFocus();
		if (c == button_ok)
			button_cancel.requestFocus();
	}
}
