package Vanderlande.BookManagement.Logic;


public class Book
{

	private static long oid;
	//Parameter sollten private sein, getter/setter müssen generiert werden
	public long id;
    public String title;
    public float price;
    
    public Book(){
    	id=oid++;
    }

    public Book(String title, float price){
    	this();
    	this.title = title;
    	this.price = price;
    }
    
    public Book(long id, String title, float price){
    	this.id = id;
    	if(this.id >= oid){
    		oid = this.id + 1;
    	}
    	this.title = title;
    	this.price = price;
    }

//	public String toString() {
//		String st = "";
//		st += "ID: " + id + "  Titel: " + titel + "  Preis: " + preis;
//		return st;
//	}
	
	// Ausgabe nur gut bei Monospace-Font f�r JList
	public String toString() {
		String st = String.format("ID: %2d Titel: %-35s %8s %4.2f", id, title, " Preis: ", price);
		return st; 
	}


	/** N�tig, wenn B�cher in HashSet gespeichert werden sollen
	 * 2 B�cher werden als gleich angesehen, wenn ihre ID gleich ist (equals)
	 * Sind 2 B�cher per equals gleich, muss auch ihr Hashcode derselbe sein
	 */
	public int hashCode() {
		return new Long(id).hashCode();
	}
	
	public boolean equals(Object o) {
		Book b = (Book)o;
		return id == b.id; // Analog zu equals-Methode der Klasse Long
	}

	public String getTitle() {
		return title;
	}

	public float getPrice() {
		return price;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setPrice(float price) {
		this.price = price;
	}
}
