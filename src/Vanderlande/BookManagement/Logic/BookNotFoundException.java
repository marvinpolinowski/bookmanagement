package Vanderlande.BookManagement.Logic;

@SuppressWarnings("serial")
public class BookNotFoundException extends Exception {
	
	public BookNotFoundException() {
		super();
	}
	public BookNotFoundException(String msg) {
		super(msg);
	}

}
