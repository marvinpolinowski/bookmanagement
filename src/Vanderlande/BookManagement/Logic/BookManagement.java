package Vanderlande.BookManagement.Logic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import Vanderlande.BookManagement.Data.IBookDAO;

public class BookManagement
{
	/** Attribut für die Liste der vorhandenen B�cher */
	private Set<Book> list;

	/**
	 * Attribut für das DAO-Objekt, das den persistenten Zugriff auf die Bücher
	 * realisiert
	 */
	private IBookDAO dao;

	/**
	 * Konstruktor für die Buecherverwaltung
	 * 
	 * @param dao
	 *            DAO-Objekt zur Persistierung der Bücher
	 */
	public BookManagement(IBookDAO dao) {
		list = new HashSet<Book>();
		this.dao = dao;
	}

	/**
	 * Kopiert die Elemente der gespeicherten Buchliste in eine neue Liste und
	 * gibt die neue Liste zurück
	 * 
	 * @return List<Book> Kopie der Buchliste
	 */
	public List<Book> getBookList() {
		List<Book> listCopy = new ArrayList<Book>();
		for (Book elem : list)
			listCopy.add(elem);
		return listCopy;
	}

	/**
	 * @return true, wenn die Liste nicht leer ist, sonst false
	 */
	public boolean noBooks() {
		return list.isEmpty();
	}

	/**
	 * F�gt das als Parameter �bergebene Buch b der Buchliste hinzu
	 * 
	 * @param b
	 *            Buch, das der Liste hinzugef�gt werden soll
	 * @throws BookExistingException
	 */
	public void add(Book b) throws BookExistingException {
		if (!list.add(b)) {
			String str = "Buch kann nicht gespeichert werden,\n" + "da bereits ein Buch mit derselben ID existiert:\n"
					+ b.toString();
			throw new BookExistingException(str);
		}
	}

	/**
	 * Gibt das Buchobjekt aus der Buchliste zur�ck, dessen ID mit der
	 * �bergebenen Zahl �bereinstimmt. Gibt es kein solches Buch, wird eine
	 * Exception geworfen
	 * 
	 * @param id
	 *            ID des gesuchten Buchs
	 * @return Buch s.o.
	 * @throws BookNotFoundException
	 */
	public Book getBook(long id) throws BookNotFoundException
	{
		for (Book b : list) {
			if (b.id == id)
				return b;
		}
		throw new BookNotFoundException();
	}

	/**
	 * Löscht die Elemente aus der internen Liste und lädt die gespeicherten
	 * Bücher in die interne Liste
	 * 
	 * @throws IOException
	 * @throws BookExistingException
	 */
	public void load() throws IOException, BookExistingException {
		list.clear();
		List<Book> l = dao.load();
		try {
			// Elemente aus l kopieren in interne Struktur liste.
			// Kommen in l zwei inhaltsgleiche Objekte vor, k�nnen diese nicht
			// beide in Set eingef�gt werden. Daher wird in diesem Fall eine
			// Exception geworfen
			for (Book b : l)
				this.add(b);
		} catch (BookExistingException ex) {
			throw new BookExistingException(
					"Fehler beim Laden der Buchdaten. Es gibt zwei Bücher mit derselben ID!");
		}
	}

	/**
	 * Speichert die Bücherliste mithilfe des DAO-Objekts. Kopiert internes
	 * Set-Objekt um in List-Objekt, das zum Speichern verwendet wird
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException {
		List<Book> bookList = new ArrayList<Book>();
		for (Book b : list)
			bookList.add(b);
		dao.save(bookList);
	}
}
