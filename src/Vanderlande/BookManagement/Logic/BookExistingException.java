package Vanderlande.BookManagement.Logic;

@SuppressWarnings("serial")
public class BookExistingException extends Exception {
	
	public BookExistingException() {
		super();
	}
	
	public BookExistingException(String msg) {
		super(msg);
	}
}
